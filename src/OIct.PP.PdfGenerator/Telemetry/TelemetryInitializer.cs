using System.Reflection;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;

namespace OIct.PP.PdfGenerator.Telemetry;

public class TelemetryInitializer : ITelemetryInitializer
{
    private readonly string? _roleName;

    public TelemetryInitializer()
    {
        _roleName = Assembly.GetEntryAssembly()?.GetName().Name;
    }

    public void Initialize(ITelemetry telemetry)
    {
        if (string.IsNullOrEmpty(_roleName))
        {
            return;
        }

        telemetry.Context.Cloud.RoleName = _roleName;
    }
}