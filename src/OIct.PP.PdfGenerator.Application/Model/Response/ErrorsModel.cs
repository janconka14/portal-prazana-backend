using System.Text.Json;
using System.Text.Json.Serialization;

namespace OIct.PP.PdfGenerator.Application.Model.Response;

public class ErrorsModel
{
    [JsonPropertyName("chyby")] 
    public IEnumerable<ErrorModel> Errors { get; set; } = new List<ErrorModel>();

    public override string ToString()
    {
        return JsonSerializer.Serialize(this);
    }
}