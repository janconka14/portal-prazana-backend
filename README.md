
# Introduction

This repository contains open source components that are used in project [Portál Pražana](https://portalprazana.cz/).

  

# Build and run
Requirements:
- [.Net 6](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)

Using terminal, navigate to src/OIct.PP.<component> folder and run `dotnet run`, then navigate to the address provided in terminal output and append /swagger. For example, https://localhost:7049/swagger

We have also provided a template and sample pipeline code that could be used to build and deploy this project
  
# Contact us

The best way to contact us is by opening an issue